<?php

class UsuariosController extends BaseController {
//use Twilio;
//use Twilio\Rest\Client; 
	//require __DIR__ . '/vendor/autoload.php';

       //use Twilio\Rest\Client;
		

	public function getIndex()
	{
		return "Hola Mundo";
	}

	public function getContrasena()
	{
		return View::make('contraseña.html');
	}

	public function getAutenticarusuario($email,$password)
	{
		$retObj = new stdClass();
		$retObj ->estado =false;
		$retObj->error='Los datos de usuario y contraseña son incorrectas.';

		$email = base64_decode($email);

		//return base64_encode("ore959881650@gmail.com");


		$user= Usuarios::where("correoUsuario","=", $email)
		->where('estado','=',0)		
		->get();

		if(count($user)>0)
		{
			$retObj->estado=false;
			$retObj->error="cuenta inactiva, dirigese a su correo";
			
			return json_encode($retObj) ;
		}



		$user= Usuarios::where("correoUsuario","=", $email)		
		->select('idUsuario','passwordUsuario','estado')
		->get();
	
		 if (count($user)> 0) {
			if (Hash::check($password, $user[0]->passwordUsuario))
				{
					$retObj -> estado = true;
					$retObj -> user=Usuarios::where("correoUsuario","=", $email)		
									->select('idUsuario','nombresUsuario','apellidosUsuario','direccionUsuario','idDepartamento','idProvincia','idDistrito','celularUsuario','dniUsuario','correoUsuario','fechaNacimiento','sexoUsuario','tipoDocumento','estado')
									->first();		
				}
		}
			return json_encode($retObj) ;

		
	}

	public function postListarperiodicidad()
	{
		return Periodicidades::all() ;

	}
	public function postListarservicios()
	{
		return Servicios::all() ;

	}
	public function postListarproductos()
	{
		return Productos::all() ;

	}

	public function postListarmoneda()
	{
	
		$moneda= Parametros::where("dominio","=", 'moneda' )
		->get();
		return $moneda;

	}


	public function postListardepartamentos()
	{
		return Departamento::all() ;

	}


	public function postListarprovincias()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		
		$provincia= Provincia::where("idDepartamento","=", $data )->get() ;

		return $provincia;

	}

	public function postListardistritos()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		
		$distrito= Distrito::where("idProvincia","=", $data )->get() ;

		return $distrito;

	}
	public function postDatosbusqueda()
	{
		//$data = file_get_contents("php://input");
		$data = json_decode(file_get_contents("php://input"), true);
		
		$lista_resultado= Usuarios::where('nombresUsuario', 'LIKE', '%' . $data['query']. '%')
		->where('idUsuario','<>',$data['id'])

		->selectRaw('CONCAT(nombresUsuario, " ", apellidosUsuario) as nombresUsuarios,apellidosUsuario,nombresUsuario,correoUsuario,direccionUsuario,dniUsuario,fechaNacimiento,idUsuario,sexoUsuario')

		->get() ;

		return $lista_resultado;

	}
	public function postObtenerdatospordniingresado()
	{

		$retObj = new stdClass();
		$retObj -> success = true;
		$retObj->error="";


		$data = json_decode(file_get_contents("php://input"), true);


		 $persona = DB::table('Personas as a')
	        ->where("dniUsuario","=", $data )
	        ->selectRaw('CONCAT(a.paternoUsuario, " ", a.maternoUsuario) as apellidosUsuario,fechaNacimiento,nombresUsuario,dniUsuario,idDepartamento,idProvincia,idDistrito,sexoUsuario')
	        ->get();



		if(count($persona)<=0)
		{
			$retObj->success=false;
			$retObj->error="ingrese un DNI valido";
			
			return json_encode($retObj) ;
		}else{
			$retObj -> success = true;
		    $retObj -> usuario = $persona[0];
		     $persona[0]->fechaNacimiento=date_format(date_create($persona[0]->fechaNacimiento), 'd/m/Y');
		    return json_encode($retObj) ;

		}

	}


	public function postBuscarbenificiario()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		

		$lista_resultado = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idBeneficiario')
			->join('Cuotas as d', 'd.idPrestamo', '=', 'a.idPrestamo')
			->join('Parametros as c', 'a.monedaPrestamo', '=', 'c.codigo')
	        ->where('b.nombresUsuario', 'LIKE', '%' . $data['palabra'] . '%')
	        ->where('a.idPrestamista',$data['idusuario'])
	        ->where('d.estado','=',5)
	        ->where('c.dominio','=','simbolo_moneda')
	        ->selectRaw('CONCAT(b.nombresUsuario, " ", b.apellidosUsuario, " - " ,c.valor, " ",a.montoPrestamo) as nombresUsuarios,b.correoUsuario,b.direccionUsuario,b.dniUsuario,b.fechaNacimiento,b.idUsuario,b.sexoUsuario,a.fechaPagoPrestamo,a.idBeneficiario,a.idPrestamo,a.cuotasPrestamo,a.idPrestamista,a.interesesPrestamo,a.monedaPrestamo,a.montoPrestamo,a.periodicidadInteresPrestamo,a.periodicidadPrestamo')
	        ->groupBy('nombresUsuarios')
	        ->get();
		return $lista_resultado;

	}

	public function postBuscarprestamista()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		

		$lista_resultado = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idPrestamista')
			->join('Cuotas as d', 'd.idPrestamo', '=', 'a.idPrestamo')
			->join('Parametros as c', 'a.monedaPrestamo', '=', 'c.codigo')
	        ->where('b.nombresUsuario', 'LIKE', '%' . $data['palabra'] . '%')
	        ->where('a.idBeneficiario',$data['idusuario'])
            ->where('c.dominio','=','simbolo_moneda')
            ->where('d.estado','<>',5)
            ->where('d.estado','<>',6)
	        ->selectRaw('CONCAT(b.nombresUsuario, " ", b.apellidosUsuario, " - " ,c.valor, " ",a.montoPrestamo) as nombresUsuarios,b.apellidosUsuario,b.nombresUsuario,b.correoUsuario,b.direccionUsuario,b.dniUsuario,b.fechaNacimiento,b.idUsuario,b.sexoUsuario,a.fechaPagoPrestamo,a.idBeneficiario,a.idPrestamo,a.cuotasPrestamo,a.idPrestamista,a.interesesPrestamo,a.monedaPrestamo,a.montoPrestamo,a.periodicidadInteresPrestamo,a.periodicidadPrestamo')
	       ->groupBy('nombresUsuarios')
	        ->get();
		return $lista_resultado;

	}

	public function postBuscararrendatario()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		

		$lista_resultado = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendatario')
			->join('PagosAlquiler as d', 'd.idAlquileres', '=', 'a.idAlquiler')
			->join('Parametros as c', 'a.monedaAlquiler', '=', 'c.codigo')
	        ->where('b.nombresUsuario', 'LIKE', '%' . $data['palabra'] . '%')
	        ->where('a.idArrendador',$data['idusuario'])
	        ->where('c.dominio','=','simbolo_moneda')
	        ->where('d.estadoPagoAlquiler','=',5)
	        ->selectRaw('CONCAT(b.nombresUsuario, " ", b.apellidosUsuario, " - " ,c.valor, " ",a.montoAlquiler) as nombresUsuarios,b.correoUsuario,b.direccionUsuario,b.dniUsuario,b.fechaNacimiento,b.idUsuario,b.sexoUsuario,a.idArrendatario,a.idAlquiler,a.idArrendador,a.garantia,a.inicio,a.monedaAlquiler,a.montoAlquiler,a.periodicidad,a.periodicidadMonto,a.plazo,a.productoAlquiler,a.referencia')
	        ->groupBy('nombresUsuarios')
	        ->get();
		return $lista_resultado;

	}
	public function postBuscararrendador()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		

		$lista_resultado = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idarrendador')
			->join('PagosAlquiler as d', 'd.idAlquileres', '=', 'a.idAlquiler')
			->join('Parametros as c', 'a.monedaAlquiler', '=', 'c.codigo')
	        ->where('b.nombresUsuario', 'LIKE', '%' . $data['palabra'] . '%')
	        ->where('a.idArrendatario',$data['idusuario'])
	        ->where('c.dominio','=','simbolo_moneda')
	        ->where('d.estadoPagoAlquiler','<>',5)
            ->where('d.estadoPagoAlquiler','<>',6)
	        ->selectRaw('CONCAT(b.nombresUsuario, " ", b.apellidosUsuario, " - " ,c.valor, " ",a.montoAlquiler) as nombresUsuarios,b.correoUsuario,b.direccionUsuario,b.dniUsuario,b.fechaNacimiento,b.idUsuario,b.sexoUsuario,a.idArrendatario,a.idAlquiler,a.idArrendador,a.garantia,a.inicio,a.monedaAlquiler,a.montoAlquiler,a.periodicidad,a.periodicidadMonto,a.plazo,a.productoAlquiler,a.referencia')
            ->groupBy('nombresUsuarios')
	        ->get();
		return $lista_resultado;

	}




	public function postMired()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		
           $contratos = DB::table('usuarios as a')
           ->select('nombresUsuario','apellidosUsuario')
           ->join('Contratos as b', 'a.idUsuario', '=', 'b.idContratante')
           ->where('b.idContratado',$data)
           ->distinct();

            $alquileres = DB::table('usuarios as a')
	        ->select('nombresUsuario','apellidosUsuario')
	        ->join('Alquileres as b', 'a.idUsuario', '=', 'b.idArrendador')
	        ->where('b.idArrendatario',$data)
	        ->distinct();

	        $prestamos = DB::table('usuarios as a')
	        ->select('nombresUsuario','apellidosUsuario')
	        ->join('prestamo as b', 'a.idUsuario', '=', 'b.idPrestamista')
            ->where('b.idBeneficiario',$data)
	        ->distinct();

	         $ventas = DB::table('usuarios as a')
	        ->select('nombresUsuario','apellidosUsuario')
	        ->join('Ventas as b', 'a.idUsuario', '=', 'b.idVendedor')
            ->where('b.idComprador',$data)
	        ->distinct();

           	$resultado = $contratos
           	->union($alquileres)
           	->union($prestamos)
           	->union($ventas)
           	->get();

	        return $resultado;

	}

	//tpresto

	public function postObtenerdatospordni()
	{
		$data = file_get_contents("php://input");
		
		
		$user_data= Usuarios::where("idUsuario","=", $data)
		->selectRaw('CONCAT(nombresUsuario, " ", apellidosUsuario) as nombresUsuarios,idUsuario,nombresUsuario,apellidosUsuario,direccionUsuario,idDepartamento,idProvincia,idDistrito,celularUsuario,dniUsuario,correoUsuario,fechaNacimiento,sexoUsuario,tipoDocumento')
		->get() ;
		$retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($user_data)> 0) {
		$retObj -> estado = true;
		$retObj -> beneficiario = $user_data[0];

	}
	     return json_encode($retObj) ;

	}

	public function postObtenerprestamosusuario()
	{
		$data = json_decode(file_get_contents("php://input"), true);
	
		$user_prestamo = DB::table('prestamo as a')

   
   	
		->where('a.idBeneficiario',$data)
		->where('a.estadoPrestamo',1)
		->where('c.dominio','simbolo_moneda')
        ->join('usuarios as b', 'a.idPrestamista', '=', 'b.idUsuario')
        ->join('Parametros as c', 'c.codigo', '=', 'a.monedaPrestamo')
        ->select(DB::raw('DATE_FORMAT(a.fechaPagoPrestamo, "%d/%m/%Y") as fecha'),'b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','c.valor')
        ->get();
        /*$user_prestamo[0]->fechaPagoPrestamo=date_format(date_create($user_prestamo[0]->fechaPagoPrestamo), 'd/m/Y'));*/

        return $user_prestamo;

	}

	public function postResumenprestamorecibidosoles()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('prestamo')
	    ->where('idBeneficiario',$data)
        ->where('monedaPrestamo','S')
        ->where('estadoPrestamo','<>','2')
        ->where('estadoPrestamo','=','1')
	    ->sum('montoPrestamo');
		return $resumen_data;

	}
	public function postResumenprestamorecibidodolar()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('prestamo')
	    ->where('idBeneficiario',$data)
        ->where('monedaPrestamo','D')
        ->where('estadoPrestamo','<>','2')
        ->where('estadoPrestamo','=','1')
	    ->sum('montoPrestamo');
		return $resumen_data;

	}
	public function postResumenprestamorealizadosoles()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('prestamo')
	    ->where('idPrestamista',$data)
        ->where('monedaPrestamo','S')
        ->where('estadoPrestamo','<>','2')
        ->where('estadoPrestamo','=','1')
	    ->sum('montoPrestamo');
		return $resumen_data;

	}
	public function postResumenprestamorealizadodolar()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('prestamo')
	    ->where('idPrestamista',$data)
        ->where('monedaPrestamo','D')
        ->where('estadoPrestamo','<>','2')
        ->where('estadoPrestamo','=','1')
	    ->sum('montoPrestamo');
		return $resumen_data;

	}

	public function postDetalleprestamo()
	{
		$data = json_decode(file_get_contents("php://input"), true);

	

			if ($data ['moneda']['moneda'] == 1) {
		    $detalle_data = DB::table('prestamo as a')
		    ->join('usuarios as b', 'b.idUsuario', '=', 'a.idPrestamista')
		    ->join('Parametros as c', 'c.codigo', '=', 'a.monedaPrestamo')
	        ->where('a.idBeneficiario',$data['usuario'])
            ->where('a.monedaPrestamo','S')
            ->where('a.estadoPrestamo','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo','c.valor')
	   	    ->get();
		    return $detalle_data;	
		}else 
		if ($data ['moneda']['moneda'] == 2) {
		   $detalle_data = DB::table('prestamo as a')
		   ->join('usuarios as b', 'b.idUsuario', '=', 'a.idPrestamista')
		   ->join('Parametros as c', 'c.codigo', '=', 'a.monedaPrestamo')
	       ->where('a.idBeneficiario',$data['usuario'])
           ->where('a.monedaPrestamo','D')
           ->where('a.estadoPrestamo','<>','2')
           ->where('c.dominio','simbolo_moneda')
           ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo','c.valor')
	       ->get();
		   return $detalle_data;
		}else
		if ($data ['moneda']['moneda'] == 3) {
			$detalle_data = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idBeneficiario')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaPrestamo')
	        ->where('a.idPrestamista',$data['usuario'])
            ->where('a.monedaPrestamo','S')
            ->where('a.estadoPrestamo','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo','c.valor')
	        ->get();
		    return $detalle_data;
			
		}else
		if ($data ['moneda']['moneda'] == 4) {
			$detalle_data = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idBeneficiario')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaPrestamo')
	        ->where('a.idPrestamista',$data['usuario'])
            ->where('a.monedaPrestamo','D')
            ->where('a.estadoPrestamo','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo','c.valor')
	        ->get();
		    return $detalle_data;
			
		}
	}

	public function postConfirmarprestamo()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$confirmar_prestamo = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idPrestamista')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaPrestamo')
	        ->where('a.idBeneficiario',$data)
	        ->where('a.estadoPrestamo',0)
	        ->where('c.dominio','simbolo_moneda')
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo','c.valor')
	        ->get();
		return $confirmar_prestamo;

		
	}

	public function postConfirmarprestamodetalle()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$confirmar_prestamo_detail = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idPrestamista')
			->join('Periodicidades as c','c.idPeriodicidad','=','a.periodicidadPrestamo')
			->join('Parametros as d','d.codigo','=','a.monedaPrestamo')
	        ->where('a.idPrestamo',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','b.correoUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo','c.nombrePeriodicidad','d.valor')
	        ->get();


	    /*$confirmar_prestamo_detail[0]->fechaPagoPrestamo=date_format(date_create($confirmar_prestamo_detail[0]->fechaPagoPrestamo), 'd/m/Y');*/	

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($confirmar_prestamo_detail)> 0) {
		$retObj -> estado = true;
		$retObj -> confirmado = $confirmar_prestamo_detail[0];

	}
	     return json_encode($retObj) ;
		

	}

	public function postActualizarestadocontrato (){

	$data = json_decode(file_get_contents("php://input"), true);

	$contrato = Prestamo::find($data['id']);
	
	$contrato->estadoPrestamo=1;

	$contrato->save();

	$cuotas = Cuotas::where('idPrestamo', '=', $data)
	->update(array('estado' => 4));

	}

	public function postRechazarestadocontrato (){

	$data = json_decode(file_get_contents("php://input"), true);

	$contrato = Prestamo::find($data['origen']);
	
	$contrato->estadoPrestamo=2;

	$contrato->save();
	$nombre=$data['nombre'];
	$apellido=$data['apellido'];

	$correo = array( 'correo' => $data['correo'] );	

    Mail::send('rechazo',$data, function( $message ) use ($correo,$nombre,$apellido){
	
    $message->to($correo['correo'] )->subject('Rechazo de contrato');
  
	});

	}	

	public function postLiberarprestamo()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liberar_prestamo = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idBeneficiario')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaPrestamo')
	        ->where('a.idPrestamista',$data)
	        ->where('a.estadoPrestamo',1)
	        ->where('c.dominio','simbolo_moneda')
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo','c.valor')
	        ->get();
		return $liberar_prestamo;

	}

	public function postLiberarprestamodetalle()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liberar_prestamo_detail = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idBeneficiario')
	        ->where('a.idPrestamo',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo')
	        ->get();

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($liberar_prestamo_detail)> 0) {
		$retObj -> estado = true;
		$retObj -> liberado = $liberar_prestamo_detail[0];

	}
	     return json_encode($retObj) ;
		

	}

	public function postActualizarliberarcontrato (){

	$data = json_decode(file_get_contents("php://input"), true);

	$liberar = Prestamo::find($data['idprestamo']['id']);
	
	$liberar->estadoPrestamo=7;
	$liberar->comentarioPrestamo=$data['observacion'];

	$liberar->save();

	}

	public function postObtenerdatospordnicuotas()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$data_cuotas = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idBeneficiario')
	     
	        ->where('b.idUsuario',$data['idbeneficiario'])
	        ->where('a.idPrestamista',$data['dniprestamista'])
	        ->where('a.idPrestamo',$data['idprestamo'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','b.correoUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo')
	        ->get();

	    $retObj = new stdClass();	
	    $retObj -> estado = false;

		if (count($data_cuotas)> 0) {
		$retObj -> estado = true;
		$retObj -> nombrecuotas = $data_cuotas[0];

	}
	     return json_encode($retObj) ;
		

	}

	public function postObtenercuotas()
	{
		
		$data = json_decode(file_get_contents("php://input"), true);
		$cuotas = DB::table('Cuotas as a')
			->join('Parametros as b', 'a.moneda', '=', 'b.codigo')
			->where("idPrestamo","=", $data )
			->where('b.dominio','=','simbolo_moneda')
	        ->where('a.estado','=',5)
	        ->select('a.comentario','a.estado','a.fechaPago','a.idCuota','a.idPrestamo','a.interesCuota','a.moneda','a.montoCuota','a.numeroCuota','b.valor')
	        ->get();

	        return $cuotas;
		

	}

	
	public function postListardetalleliquidarcuota()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liquidarcuota = DB::table('prestamo as a')
			->join('usuarios as b', 'a.idBeneficiario', '=', 'b.idUsuario')
			->join('Cuotas as c', 'c.idPrestamo', '=', 'a.idPrestamo')
	        ->where('c.idCuota',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo','c.idCuota','c.interesCuota','c.moneda','c.montoCuota','c.numeroCuota','c.fechaPago')
	        ->get();

	    $liquidarcuota[0]->fechaPago=date_format(date_create($liquidarcuota[0]->fechaPago), 'd/m/Y');

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($liquidarcuota)> 0) {
		$retObj -> estado = true;
		$retObj -> liquidar = $liquidarcuota[0];

	}
	     return json_encode($retObj) ;


	}

	public function postActualizarliquidarcuota (){

	$data = json_decode(file_get_contents("php://input"), true);

	$liquidar = Cuotas::find($data['idcuota']['id']);
	$liquidar->comentario=$data['observacion'];
	$liquidar->estado=6;

	$liquidar->save();

	$nombre=$data['nombre'];
	$apellido=$data['apellido'];
	$numerocuota=$data['numerocuota'];

	$correo = array( 'correo' => $data['correo'] );	

    Mail::send('liquidezcuota',$data, function( $message ) use ($correo,$nombre,$apellido,$numerocuota){
	
    $message->to($correo['correo'] )->subject('Liquidez de cuota');
  
	});

	}


	public function postObtenerdatospordnipagos()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$data_cuotas = DB::table('prestamo as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idPrestamista')
	        ->where('b.idUsuario',$data['nombreprestamista'])
	        ->where('a.idBeneficiario',$data['idbeneficiario'])
	        ->where('a.idPrestamo',$data['idprestamo'])

	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','b.correoUsuario','b.celularUsuario','a.idPrestamo','a.estadoPrestamo','a.estadoPrestamo','a.idBeneficiario','a.idPrestamista','a.interesesPrestamo','a.monedaPrestamo','a.montoPrestamo','a.fechaPagoPrestamo','a.cuotasPrestamo','a.periodicidadInteresPrestamo','a.periodicidadPrestamo','a.tasaInteresPrestamo')
	        ->get();

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($data_cuotas)> 0) {
		$retObj -> estado = true;
		$retObj -> pagarcuotas = $data_cuotas[0];

	}
	     return json_encode($retObj) ;
		
		

	}

	public function postObtenercuotasdepago()
	{
		
		$data = json_decode(file_get_contents("php://input"), true);
		$cuotas = DB::table('Cuotas as a')
			->join('Parametros as b', 'a.moneda', '=', 'b.codigo')
			->where("a.idPrestamo","=", $data )
			->where('b.dominio','=','simbolo_moneda')
	        ->where('a.estado','=',4)
	        ->select('a.comentario','a.estado','a.fechaPago','a.idCuota','a.idPrestamo','a.interesCuota','a.moneda','a.montoCuota','a.numeroCuota','b.valor')
	        ->get();

	        return $cuotas;

	}

	public function postNumerocuota()
	{
		

		$data = json_decode(file_get_contents("php://input"), true);
		$cuotas= Cuotas::where("idCuota","=", $data['id'] )
		->select('numeroCuota')
		->first();
		return $cuotas;

	}

	

	public function postPagarcuota()
	{
		
		$data = json_decode(file_get_contents("php://input"), true);
		$pagocuota = Cuotas::find($data['idcuota']['id']);
		$pagocuota->estado = 5;
		$pagocuota->save();

		$pago =  new Pagos;
		$pago->idCuota=$data['idcuota']['id'];
		$pago->formaPago=$data['pago']['forma'];
		$pago->monedaPago=$data['pago']['moneda'];
		$pago->montoPago=$data['pago']['monto'];
		$conversion= $data['pago']['fecha'];
		$date = DateTime::createFromFormat('d/m/Y', $conversion);
		$pago->fechaPago = $date;
		$pago->save();

		$nombre=$data['nombre'];
		$apellido=$data['apellido'];
		$numerocuota=$data['numerocuota'];
		$celular=$data['celular'];
		$celularcodigo= '+51';

		Twilio::sendMessage($celularcodigo.$celular, " El beneficiario "     . $nombre  . $apellido .  " pago el numero de cuota " . $numerocuota ) ;
		
		$correo = array( 'correo' => $data['correo'] );	

        Mail::send('pagocuota',$data, function( $message ) use ($correo,$nombre,$apellido,$numerocuota){
	
        $message->to($correo['correo'] )->subject('Pago de cuota');
  
	    });
	}
	
	 public function postVerificarcorreo()
 	{
	$data = json_decode(file_get_contents("php://input"), true);

	$user= Usuarios::where("correoUsuario","=", $data['correo'])->first() ;

	$retObj = new stdClass();
	$retObj -> success = false;

	if (count($user)> 0) {
		$tokenrandom = str_random(100);

	$token =  new Token;
		$token->idUsuario = $user ->idUsuario;
		$token->valor = $tokenrandom;
		$token->fechaCaducidad = "16/08/2017";
		$token->idEstado = 8;
		$token->FechaCreacion = "16/08/2017";

		$token->save();

	$correo = array( 'correo' => $data['correo'] );
	$data["token"]=$tokenrandom;	

    Mail::send('message',$data, function( $message ) use ($correo,$tokenrandom){
	
    $message->to($correo['correo'] )->subject('cambio de contraseña');
  
	});

	$retObj -> success = true;

	}

	return json_encode($retObj) ;			

    }

public function postVerificartoken(){

$data = json_decode(file_get_contents("php://input"), true);

$listatoken= Token::where("valor","=",$data['token'])
->select('idToken','idEstado','idUsuario','valor','fechaCaducidad','FechaCreacion')
->where("idEstado","=",8)
->get() ;

$retObj = new stdClass();
$retObj -> estado = false;

//count ==  cuantos elementos tiene el aray

	if (count($listatoken)> 0) {
		$retObj -> estado = true;
		$retObj -> token = $listatoken[0];

	}
	return json_encode($retObj) ;

}

public function postActualizarpasswordtoken(){

$data = json_decode(file_get_contents("php://input"), true);

$listatoken = Token::where("valor","=",$data['token'])->first() ;

$retObj = new stdClass();
	
$password = $data['password'];
$confirmarpassword = $data['passwordconfirmado'];

  	if ($password != $confirmarpassword) {
	$retObj -> success = false;
	return json_encode($retObj);		
  	}

	//count ==  cuantos elementos tiene el aray

	if (count($listatoken)> 0) {
		
		$password = $data['password'];
		$confirmarpassword = $data['passwordconfirmado'];
		$id = $listatoken->idUsuario;

		$user = Usuarios::find($id);
		$password = Hash::make($data['password']);
		$user->passwordUsuario = $password;
		$user->save();

		$idtoken = Token::where('valor', '=', $data['token'])
	    ->update(array('idEstado' => 9));


		$retObj -> success = true;
	}

	return json_encode($retObj) ;


}

//activar cuenta

public function postVerificartokencuenta(){

$data = json_decode(file_get_contents("php://input"), true);

$listatoken= Token::where("valor","=",$data['token'])
->select('idToken','idEstado','idUsuario','valor','fechaCaducidad','FechaCreacion')
->where("idEstado","=",8)
->get() ;

$retObj = new stdClass();
$retObj -> estado = false;

//count ==  cuantos elementos tiene el aray

	if (count($listatoken)> 0) {
		$retObj -> estado = true;
		$retObj -> token = $listatoken[0];

	}
	return json_encode($retObj) ;

}

public function postActualizarestadotoken(){

$data = json_decode(file_get_contents("php://input"), true);

$listatoken = Token::where("valor","=",$data['token'])->first() ;

$retObj = new stdClass();
		$id = $listatoken->idUsuario;
		$user = Usuarios::find($id);
		$user->estado = 1;
		$user->save();

		$idtoken = Token::where('valor', '=', $data)
	    ->update(array('idEstado' => 9));

		$retObj -> success = true;
	

	return json_encode($retObj) ;


}


//

public function postGuardarusuario()
	{

		$retObj = new stdClass();
		$retObj -> success = true;
		$retObj->error="";
		$data = json_decode(file_get_contents("php://input"), true);

		$user_dni= Usuarios::where("dniUsuario","=", $data['dniUsuario'])->get() ;

		if(count($user_dni)>0)
		{
			$retObj->success=false;
			$retObj->error="Ya existe un usuario con el mismo dni";
			
			return json_encode($retObj) ;
		}

		$persona_dni= Personas::where("dniUsuario","=", $data['dniUsuario'])->get() ;

		if(count($persona_dni)<=0)
		{
			$retObj->success=false;
			$retObj->error="Ingrese un DNI valido";
			
			return json_encode($retObj) ;
		}

		$user_correos= Usuarios::where("correoUsuario","=", $data['email'])->get() ;

		if(count($user_correos)>0)
		{
			$retObj->success=false;
			$retObj->error="Ya existe un usuario con el mismo correo";
			
			return json_encode($retObj) ;
		}

				//validaciones de los campos del form
		//nombre
		
		
		$nombre = $data['nombresUsuario'];
		$borrarespaciosvacios = trim($nombre);
		if (!preg_match("#[a-zA-Z]+#", $nombre)) {
       	$retObj->success=false;
		$retObj->error="el nombre solo debe contener solo letras";
		return json_encode($retObj) ;
    	} 

    	//apellido	
    	$apellido = $data['apellidosUsuario'];
    	$borrarespaciosvacios = trim($apellido);
		if (!preg_match("#[a-zA-Z]+#", $apellido)) {
       	$retObj->success=false;
		$retObj->error="el apellido solo debe contener solo letras";
		return json_encode($retObj) ;
    	}

    	//dni
    	$dni = $data['dniUsuario'];
    	$borrarespaciosvacios = trim($dni);

    	$dnicorrecto = $data['dniUsuario'];
		$borrarespaciosvacios = trim($dnicorrecto);
		
    	//login
    	/*$login = $data['login'];
    	$borrarespaciosvacios = trim($login);*/

    	//password	
		$pwd = $data['password'];
		$borrarespaciosvacios = trim($pwd);
		if (strlen($pwd) < 8) {
        $retObj->success=false;
		$retObj->error="la contraseña es muy corto";	
		return json_encode($retObj) ;
    	}

    	if (!preg_match("#[0-9]+#", $pwd)) {
        $retObj->success=false;
		$retObj->error="La contraseña debe incluir al menos un número!";
		return json_encode($retObj) ;
    	}

    	if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        $retObj->success=false;
		$retObj->error="La contraseña debe incluir al menos una letra!";	
		return json_encode($retObj) ;
    	}   

    	//fecha de nacimiento
    	$hoy = date("Y");
    	$Year=date("Y",strtotime($data['fechaNacimiento']));
    	$resta = $hoy - $Year;

		if ($resta <18) {
       	$retObj->success=false;
		$retObj->error="solo pueden ingresar mayores de 18 años";
		return json_encode($retObj) ;
    	} 
  

  		$password = $data['password'];
  		$password1 = $data['password1'];

  		if ($password != $password1) {
  		$retObj->success=false;
		$retObj->error="Las contraseñas no coinciden";
		return json_encode($retObj) ;
  			
  		}


    	//guardado de usuario correcto	
		$usuario =  new Usuarios;

		$usuario->nombresUsuario = $data['nombresUsuario'];
		$usuario->apellidosUsuario = $data['apellidosUsuario'];
		$usuario->correoUsuario = $data['email'];
		$usuario->direccionUsuario = $data['direccion'];

		$password = Hash::make($data['password']);
		$usuario->passwordUsuario = $password;

		$usuario->celularUsuario = $data['celular'];
		$usuario->idDepartamento = $data['idDepartamento'];
		$usuario->idProvincia = $data['idProvincia'];
		$usuario->idDistrito = $data['idDistrito'];
		$usuario->tipoDocumento = $data['tipoDocumento'];
		$usuario->dniUsuario = $data['dniUsuario'];

		$conversion= $data['fechaNacimiento'];
		$date = DateTime::createFromFormat('d/m/Y', $conversion);
		$usuario->fechaNacimiento = $date;

		$usuario->sexoUsuario = $data['sexoUsuario'];

		$usuario->save();


	$tokenrandom = str_random(100);

	$token =  new Token;
		$token->idUsuario = $usuario ->idUsuario;
		$token->valor = $tokenrandom;
		$token->fechaCaducidad = "16/08/2017";
		$token->idEstado = 8;
		$token->FechaCreacion = "16/08/2017";

		$token->save();

	$correo = array( 'correo' => $data['email'] );
	$data["token"]=$tokenrandom;	

    Mail::send('habilitado',$data, function( $message ) use ($correo,$tokenrandom){
	
    $message->to($correo['correo'] )->subject('Enhorabuena, ¡ya estás en Confianza! Por seguridad, confirma tu dirección email.');
  
	});


	return json_encode($retObj) ;
	}


	public function postActualizarusuario()
	{

		$data = json_decode(file_get_contents("php://input"), true);

		$user_id = $data['idUsuario'];

		$user= Usuarios::where("idUsuario","=", $data['idUsuario'])
		->select('idUsuario','nombresUsuario','apellidosUsuario','correoUsuario','direccionUsuario','celularUsuario','idDepartamento','idProvincia','idDistrito','tipoDocumento','dniUsuario','fechaNacimiento','sexoUsuario')
		->get() ;

		$retObj = new stdClass();
		$retObj -> success = true;
		$retObj -> nota = 'aqui debe mandar usuario';
		$retObj -> user = $user[0];
		$retObj->error="";

		$user_dni= Usuarios::where("dniUsuario","=", $data['dniUsuario'])
		->where("idUsuario","<>",$user_id)
		->get() ;

		if(count($user_dni)>0)
		{
			$retObj->success=false;
			$retObj->error="Ya existe un usuario con el mismo dni";
			return json_encode($retObj) ;
		}


		$user_correos= Usuarios::where("correoUsuario","=", $data['correoUsuario'])
		->where("idUsuario","<>",$user_id)
		->get() ;

		if(count($user_correos)>0)
		{
			$retObj->success=false;
			$retObj->error="Ya existe un usuario con el mismo correo";
			return json_encode($retObj) ;
		}

		//validaciones de los campos del form
		//nombre
		
		
		$nombre = $data['nombresUsuario'];
		$borrarespaciosvacios = trim($nombre);
		if (!preg_match("#[a-zA-Z]+#", $nombre)) {
       	$retObj->success=false;
		$retObj->error="el nombre solo debe contener solo letras";
		return json_encode($retObj) ;
    	} 

    	//apellido	
    	$apellido = $data['apellidosUsuario'];
    	$borrarespaciosvacios = trim($apellido);
		if (!preg_match("#[a-zA-Z]+#", $apellido)) {
       	$retObj->success=false;
		$retObj->error="el apellido solo debe contener solo letras";
		return json_encode($retObj) ;
    	}

    	//dni
    	$dni = $data['dniUsuario'];
    	$borrarespaciosvacios = trim($dni);

    	$dnicorrecto = $data['dniUsuario'];
		$borrarespaciosvacios = trim($dnicorrecto);
		
    	//login
    	
    	if($data['checkpassword'] == true){
    	$pwd = $data['passwordUsuario'];
		$borrarespaciosvacios = trim($pwd);
		if (strlen($pwd) < 8) {
        $retObj->success=false;
		$retObj->error="la contraseña es muy corto";	
		return json_encode($retObj) ;
    	}

    	if (!preg_match("#[0-9]+#", $pwd)) {
        $retObj->success=false;
		$retObj->error="La contraseña debe incluir al menos un número!";
		return json_encode($retObj) ;
    	}

    	if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        $retObj->success=false;
		$retObj->error="La contraseña debe incluir al menos una letra!";	
		return json_encode($retObj) ;
    	}  
    	}
    	//password	
		

    	//fecha de nacimiento
    	$hoy = date("Y");
    	$Year=date("Y",strtotime($data['fechaNacimiento']));
    	$resta = $hoy - $Year;

		if ($resta <18) {
       		$retObj->success=false;
			$retObj->error="solo pueden ingresar mayores de 18 años";
			return json_encode($retObj) ;
    	} 
  

    	if ($data['checkpassword'] == true) {
    		$password = $data['passwordUsuario'];
  			$passwordconfirmado = $data['passwordUsuarioconfirmar'];

  			if ($password != $passwordconfirmado) {
  				$retObj->success=false;
				$retObj->error="Las contraseñas no coinciden";
				return json_encode($retObj) ;
  			
  			}

    	}
  		

    	//guardado de usuario correcto	
		

		$usuario = Usuarios::find($user_id);
		$usuario->nombresUsuario = $data['nombresUsuario'];
		$usuario->apellidosUsuario = $data['apellidosUsuario'];
		$usuario->correoUsuario = $data['correoUsuario'];
		$usuario->direccionUsuario = $data['direccionUsuario'];

   		if ($data['checkpassword'] == true) {

   			$password = Hash::make($data['passwordUsuario']);
   			$usuario->passwordUsuario = $password;
   		}
		
		$usuario->celularUsuario = $data['celularUsuario'];
		$usuario->idDepartamento = $data['idDepartamento'];
		$usuario->idProvincia = $data['idProvincia'];
		$usuario->idDistrito = $data['idDistrito'];
		$usuario->tipoDocumento = $data['tipoDocumento'];
		$usuario->dniUsuario = $data['dniUsuario'];

		$conversion= $data['fechaNacimiento'];
		$date = DateTime::createFromFormat('d/m/Y', $conversion);
		$usuario->fechaNacimiento = $date;
		
		$usuario->sexoUsuario = $data['sexoUsuario'];
		$usuario->save();

		

	return json_encode($retObj) ;
	}
   
   
public function postRecibirid(){

$data = json_decode(file_get_contents("php://input"), true);
$user= Usuarios::where("idUsuario","=",$data)
->select('idUsuario','nombresUsuario','apellidosUsuario','correoUsuario','direccionUsuario','celularUsuario','idDepartamento','idProvincia','idDistrito','tipoDocumento','dniUsuario','fechaNacimiento','sexoUsuario')
->get();

 $user[0]->fechaNacimiento=date_format(date_create($user[0]->fechaNacimiento), 'd/m/Y');
 	 
	if(count($user)>0)
	{
		return $user[0];
	}

	return "{}";

}

public function postGuardarprestamo()
	{
		$data = json_decode(file_get_contents("php://input"), true);

		//return $data['beneficiario']['dniUsuario'] ;	
			//return $data;

			$prestamo =  new Prestamo;
		    $prestamo->idPrestamista = $data['prestatario']['idUsuario'];
		    $prestamo->idBeneficiario = $data['beneficiario']['idUsuario'];
		    $prestamo->montoPrestamo = $data['monto'];
		    $prestamo->monedaPrestamo = $data['moneda'] ['codigo'];
		    $prestamo->cuotasPrestamo = $data['nro_cuotas'];
		    $prestamo->periodicidadPrestamo = $data['periodo_cuotas'] ['idPeriodicidad'];

		    if ($data['checkinteres'] == true) {
   			    $prestamo->tasaInteresPrestamo = $data['tasa'];
		        $prestamo->periodicidadInteresPrestamo = $data['periodo_tasa'] ['codigo'];
		        $prestamo->interesesPrestamo = 'T';
   		    }else{
   		    	$prestamo->interesesPrestamo = 'F';
   		    }

		    $conversion= $data['fecha_pago'];
		    $date = DateTime::createFromFormat('d/m/Y', $conversion);
		    $prestamo->fechaPagoPrestamo = $date;
		    $prestamo->estadoPrestamo =0;

		    $prestamo->save();

		    for ($i = 0; $i < $data['nro_cuotas']; $i++){

		        $cuota = new Cuotas;

		        $cuota->idPrestamo=$prestamo->idPrestamo;
		        $cuota->numeroCuota=$i+1;
		        $cuota->montoCuota =$data['monto']/$data['nro_cuotas'];
		        $cuota->moneda =$data['moneda'] ['codigo'];
		  
		        $conversion= $data['fecha_pago'];
		        $date = DateTime::createFromFormat('d/m/Y', $conversion);
		        $cuota->fechaPago =$date;
		        $cuota->comentario ='esta bien';
		        $cuota->estado = 3;

		        if ($data['checkinteres'] == true) {
		        	$cuota->interesCuota = $data['tasa']/$data['nro_cuotas'];
   			    
   		        }
		       

		        $cuota->save();
		    }


			return 'true';

		
	}

	//envio de correo al registrar

	//tcontrato
	public function postResumencontratorecibidosoles()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Contratos')
	    ->where('idContratado',$data)
        ->where('monedaContrato','S')
        ->where('estado_Contrato','<>','2')
        ->where('estado_Contrato','=','1')
	    ->sum('montoContrato');
		return $resumen_data;

	}
	public function postResumencontratorecibidodolar()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Contratos')
	    ->where('idContratado',$data)
        ->where('monedaContrato','D')
        ->where('estado_Contrato','<>','2')
        ->where('estado_Contrato','=','1')
	    ->sum('montoContrato');
		return $resumen_data;

	}
	public function postResumencontratorealizadosoles()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Contratos')
	    ->where('idContratante',$data)
        ->where('monedaContrato','S')
        ->where('estado_Contrato','<>','2')
        ->where('estado_Contrato','=','1')
	    ->sum('montoContrato');
		return $resumen_data;

	}
	public function postResumencontratorealizadodolar()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Contratos')
	    ->where('idContratante',$data)
        ->where('monedaContrato','D')
        ->where('estado_Contrato','<>','2')
        ->where('estado_Contrato','=','1')
	    ->sum('montoContrato');
		return $resumen_data;

	}

	public function postDetallecontrato()
	{
		$data = json_decode(file_get_contents("php://input"), true);

		

		if ($data['moneda'] ['moneda'] == 1) {
		    $detalle_data = DB::table('Contratos as a')
		    ->join('usuarios as b', 'b.idUsuario', '=', 'a.idContratante')
		    ->join('Parametros as c', 'c.codigo', '=', 'a.monedaContrato')
	        ->where('a.idContratado',$data['usuario'])
            ->where('a.monedaContrato','S')
            ->where('a.estado_Contrato','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato','c.valor')
	   	    ->get();
		    return $detalle_data;	
		}else 
		if ($data['moneda'] ['moneda'] == 2) {
		   $detalle_data = DB::table('Contratos as a')
		   ->join('usuarios as b', 'b.idUsuario', '=', 'a.idContratante')
		   ->join('Parametros as c', 'c.codigo', '=', 'a.monedaContrato')
	       ->where('a.idContratado',$data['usuario'])
           ->where('a.monedaContrato','D')
           ->where('a.estado_Contrato','<>','2')
           ->where('c.dominio','simbolo_moneda')
           ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato','c.valor')
	       ->get();
		   return $detalle_data;
		}else
		if ($data['moneda'] ['moneda'] == 3) {
			$detalle_data = DB::table('Contratos as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idContratado')
			 ->join('Parametros as c', 'c.codigo', '=', 'a.monedaContrato')
	        ->where('a.idContratante',$data['usuario'])
            ->where('a.monedaContrato','S')
            ->where('a.estado_Contrato','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato','c.valor')
	        ->get();
		    return $detalle_data;
					
		}else
		if ($data['moneda'] ['moneda'] == 4) {
			$detalle_data = DB::table('Contratos as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idContratado')
			 ->join('Parametros as c', 'c.codigo', '=', 'a.monedaContrato')
	        ->where('a.idContratante',$data['usuario'])
            ->where('a.monedaContrato','D')
            ->where('a.estado_Contrato','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato','c.valor')
	        ->get();
		    return $detalle_data;
			
		}
	}

	public function postObtenerdatospordnicontrato()
	{
		$data = file_get_contents("php://input");
		
		
		$user_data= Usuarios::where("idUsuario","=", $data )
		->selectRaw('CONCAT(nombresUsuario, " ", apellidosUsuario) as nombresUsuarios,idUsuario,nombresUsuario,apellidosUsuario,direccionUsuario,idDepartamento,idProvincia,idDistrito,celularUsuario,dniUsuario,correoUsuario,fechaNacimiento,sexoUsuario,tipoDocumento')
		->get() ;
		$retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($user_data)> 0) {
		$retObj -> estado = true;
		$retObj -> contratado = $user_data[0];

	}
	     return json_encode($retObj) ;

	}

	public function postObtenercontratosusuario()
	{
		$data = json_decode(file_get_contents("php://input"), true);
	
		$user_contrato = DB::table('Contratos as a')
		->where('a.idContratado',$data)
		->where('a.estado_Contrato',1)
        ->join('usuarios as b', 'a.idContratante', '=', 'b.idUsuario')
        ->join('Servicios as c','a.idServicio','=','c.idServicio')
        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato','c.nombreServicio')
        ->get();
        /*$user_contrato[0]->fechaPagoPrestamo=date_format(date_create($user_contrato[0]->fechaPagoPrestamo), 'd/m/Y');*/

        return $user_contrato;

	}

	public function postGuardarcontrato()
	{
		$data = json_decode(file_get_contents("php://input"), true);


			$contrato =  new Contratos;
		    $contrato->idContratante = $data['contratante']['idUsuario'];
		    $contrato->idContratado = $data['contratado']['idUsuario'];
		    $contrato->idServicio = $data['servicios'] ['idServicio'];
		    
		    $contrato->plazoContrato = $data['plazo'];
		    $contrato->montoContrato = $data['monto'];
		    $contrato->monedaContrato = $data['moneda'] ['codigo'];
		    $conversion= $data['inicio'];
		    $date = DateTime::createFromFormat('d/m/Y', $conversion);
		    $contrato->inicioContrato = $date;
		    $contrato->adelantoContrato = $data['adelanto'];
		    $contrato->pendienteContrato = $data['pendiente'];
		    $contrato->referenciaContrato = $data['referencia'];
		    $contrato->estado_Contrato = 0;

		    $contrato->save();

		   


			return 'true';

		
	}

	public function postConfirmarcontrato()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$confirmar_contrato = DB::table('Contratos as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idContratante')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaContrato')
	        ->where('a.idContratado',$data)
	        ->where('a.estado_Contrato',0)
	        ->where('c.dominio','simbolo_moneda')
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato','c.valor')
	        ->get();
		return $confirmar_contrato;

	}

	public function postConfirmarcontratodetalle()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$confirmar_contrato_detail = DB::table('Contratos as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idContratante')
			->join('Servicios as c','c.idServicio','=','a.idServicio')
			->join('Parametros as d','d.codigo','=','a.monedaContrato')
	        ->where('a.idContrato',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','b.correoUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato','c.nombreServicio','d.valor')
	        ->get();

	    $confirmar_contrato_detail[0]->inicioContrato=date_format(date_create($confirmar_contrato_detail[0]->inicioContrato), 'd/m/Y');

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($confirmar_contrato_detail)> 0) {
		$retObj -> estado = true;
		$retObj -> confirmado = $confirmar_contrato_detail[0];

	}
	     return json_encode($retObj) ;
		
		

	}

	public function postActualizarestadodecontrato (){

	$data = json_decode(file_get_contents("php://input"), true);

	$contrato = Contratos::find($data['origen']['id']);
	
	$contrato->estado_Contrato=1;

	$contrato->save();

	
	$nombrecontratado=$data['nombrecontratado'];
	$apellidocontratado=$data['apellidocontratado'];
	$correocontratista = array( 'correocontratista' => $data['correocontratista'] );	

    Mail::send('confirmarcontratista',$data, function( $message ) use ($correocontratista,$nombrecontratado,$apellidocontratado){
	
    $message->to($correocontratista['correocontratista'] )->subject('Se confirmo el contrato');
  
	});

	

	$nombrecontratista=$data['nombrecontratista'];
	$apellidocontratista=$data['apellidocontratista'];

	$correocontratado = array( 'correocontratado' => $data['correocontratado'] );	

    Mail::send('confirmarcontratado',$data, function( $message ) use ($correocontratado,$nombrecontratista,$apellidocontratista){
	
    $message->to($correocontratado['correocontratado'] )->subject('Se confirmo el contrato');
  
	});


	}
	public function postRechazarestadodecontrato (){

	$data = json_decode(file_get_contents("php://input"), true);

	$contrato = Contratos::find($data['origen'] ['id']);
	
	$contrato->estado_Contrato=2;

	$contrato->save();

	$nombrecontratado=$data['nombrecontratado'];
	$apellidocontratado=$data['apellidocontratado'];
	$correocontratista = array( 'correocontratista' => $data['correocontratista'] );	

    Mail::send('rechazarcontratista',$data, function( $message ) use ($correocontratista,$nombrecontratado,$apellidocontratado){
	
    $message->to($correocontratista['correocontratista'] )->subject('Se rechazo el contrato');
  
	});

	

	$nombrecontratista=$data['nombrecontratista'];
	$apellidocontratista=$data['apellidocontratista'];

	$correocontratado = array( 'correocontratado' => $data['correocontratado'] );	

    Mail::send('rechazarcontratado',$data, function( $message ) use ($correocontratado,$nombrecontratista,$apellidocontratista){
	
    $message->to($correocontratado['correocontratado'] )->subject('Se rechazo el contrato');
  
	});




	}	

	//
	public function postLiberarcontrato()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liberar_contrato = DB::table('Contratos as a' )
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idContratado')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaContrato')
	        ->where('a.idContratante',$data)
	        ->where('a.estado_Contrato',1)
	        ->where('c.dominio','simbolo_moneda')
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato','c.valor')
	        ->get();
		return $liberar_contrato;

	}

	public function postLiberarcontratodetalle()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liberar_contrato_detail = DB::table('Contratos as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idContratado')
	        ->where('a.idContrato',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idContrato','a.adelantoContrato','a.estado_Contrato','a.idContratado','a.idContratante','a.idServicio','a.inicioContrato','a.monedaContrato','a.montoContrato','a.pendienteContrato','a.plazoContrato','a.referenciaContrato')
	        ->get();

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($liberar_contrato_detail)> 0) {
		$retObj -> estado = true;
		$retObj -> liberado = $liberar_contrato_detail[0];

	}
	     return json_encode($retObj) ;
		

	}

	public function postActualizarliberardecontrato (){

	$data = json_decode(file_get_contents("php://input"), true);

	$liberar = Contratos::find($data['idContrato']['id']);
	
	$liberar->estado_Contrato=7;
	$liberar->comentarioContrato=$data['observacion'];

	$liberar->save();

	}

	//tvendo
	public function postResumenventarecibidosoles()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Ventas')
	    ->where('idComprador',$data)
        ->where('monedaVenta','S')
        ->where('estadoVenta','<>','2')
        ->where('estadoVenta','=','1')
	    ->sum('montoVenta');
		return $resumen_data;

	}
	public function postResumenventarecibidodolar()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Ventas')
	    ->where('idComprador',$data)
        ->where('monedaVenta','D')
        ->where('estadoVenta','<>','2')
        ->where('estadoVenta','=','1')
	    ->sum('montoVenta');
		return $resumen_data;

	}
	public function postResumenventarealizadosoles()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Ventas')
	    ->where('idVendedor',$data)
        ->where('monedaVenta','S')
        ->where('estadoVenta','<>','2')
        ->where('estadoVenta','=','1')
	    ->sum('montoVenta');
		return $resumen_data;

	}
	public function postResumenventarealizadodolar()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Ventas')
	    ->where('idVendedor',$data)
        ->where('monedaVenta','D')
        ->where('estadoVenta','<>','2')
        ->where('estadoVenta','=','1')
	    ->sum('montoVenta');
		return $resumen_data;

	}

	public function postDetalleventa()
	{
		$data = json_decode(file_get_contents("php://input"), true);

		

		if ($data['moneda'] ['moneda'] == 1) {
		    $detalle_data = DB::table('Ventas as a')
		    ->join('usuarios as b', 'b.idUsuario', '=', 'a.idVendedor')
		    ->join('Parametros as c', 'c.codigo', '=', 'a.monedaVenta')
	        ->where('a.idComprador',$data['usuario'])
            ->where('a.monedaVenta','S')
            ->where('a.estadoVenta','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto','c.valor')
	   	    ->get();
		    return $detalle_data;	
		}else 
		if ($data['moneda'] ['moneda'] == 2) {
		   $detalle_data = DB::table('Ventas as a')
		   ->join('usuarios as b', 'b.idUsuario', '=', 'a.idVendedor')
		   ->join('Parametros as c', 'c.codigo', '=', 'a.monedaVenta')
	       ->where('a.idComprador',$data['usuario'])
           ->where('a.monedaVenta','D')
           ->where('a.estadoVenta','<>','2')
           ->where('c.dominio','simbolo_moneda')
           ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto','c.valor')
	       ->get();
		   return $detalle_data;
		}else
		if ($data['moneda'] ['moneda'] == 3) {
			$detalle_data = DB::table('Ventas as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idComprador')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaVenta')
	        ->where('a.idVendedor',$data['usuario'])
            ->where('a.monedaVenta','S')
            ->where('a.estadoVenta','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto','c.valor')
	        ->get();
		    return $detalle_data;
					
		}else
		if ($data['moneda'] ['moneda'] == 4) {
			$detalle_data = DB::table('Ventas as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idComprador')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaVenta')
	        ->where('a.idVendedor',$data['usuario'])
            ->where('a.monedaVenta','D')
            ->where('a.estadoVenta','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto','c.valor')
	        ->get();
		    return $detalle_data;
			
		}
	}

	public function postObtenerdatospordniventa()
	{
		$data = file_get_contents("php://input");
		
		
		$user_data= Usuarios::where("idUsuario","=", $data )
		->selectRaw('CONCAT(nombresUsuario, " ", apellidosUsuario) as nombresUsuarios,idUsuario,nombresUsuario,apellidosUsuario,direccionUsuario,idDepartamento,idProvincia,idDistrito,celularUsuario,dniUsuario,correoUsuario,fechaNacimiento,sexoUsuario,tipoDocumento')
		->get() ;
		$retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($user_data)> 0) {
		$retObj -> estado = true;
		$retObj -> comprador = $user_data[0];

	}
	     return json_encode($retObj) ;

	}

	public function postObtenerventasusuario()
	{
		$data = json_decode(file_get_contents("php://input"), true);
	
		$user_ventas = DB::table('Ventas as a')
		->where('a.idComprador',$data)
		->where('a.estadoVenta',1)
        ->join('usuarios as b', 'a.idVendedor', '=', 'b.idUsuario')
        ->join('Productos as c','c.idProducto','=','a.idProducto')
        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto','c.nombreProducto')
        ->get();
        /*$user_ventas[0]->fechaPagoPrestamo=date_format(date_create($user_ventas[0]->fechaPagoPrestamo), 'd/m/Y');*/

        return $user_ventas;

	}

	public function postGuardarventa()
	{
		$data = json_decode(file_get_contents("php://input"), true);


			$venta =  new Ventas;
		    $venta->idVendedor = $data['vendedor']['idUsuario'];
		    $venta->idComprador = $data['comprador']['idUsuario'];
		    $venta->idProducto = $data['productos'] ['idProducto'];
		    $venta->montoventa = $data['monto'];
		    $venta->monedaventa = $data['moneda'] ['codigo'];
		    $conversion= $data['fecha'];
		    $date = DateTime::createFromFormat('d/m/Y', $conversion);
		    $venta->fechaVenta = $date;
		    $venta->estadoVenta = 0;

		    $venta->save();

		   


			return 'true';

	
		}

	public function postConfirmarventa()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$confirmar_venta = DB::table('Ventas as a' )
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idVendedor')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaVenta')
	        ->where('a.idComprador',$data)
	        ->where('a.estadoVenta',0)
	        ->where('c.dominio','simbolo_moneda')
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto','c.valor')
	        ->get();
		return $confirmar_venta;


	
	}

	public function postConfirmarventadetalle()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$confirmar_venta_detail = DB::table('Ventas as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idVendedor')
			->join('Productos as c','c.idProducto','=','a.idProducto')
			->join('Parametros as d','d.codigo','=','a.monedaventa')
	        ->where('a.idVenta',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','b.correoUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto','c.nombreProducto','d.valor')
	        ->get();

	   $confirmar_venta_detail[0]->fechaVenta=date_format(date_create($confirmar_venta_detail[0]->fechaVenta), 'd/m/Y');

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($confirmar_venta_detail)> 0) {
		$retObj -> estado = true;
		$retObj -> confirmado = $confirmar_venta_detail[0];

	}
	     return json_encode($retObj) ;
		
		

	}

	public function postActualizarestadodeventa (){

	$data = json_decode(file_get_contents("php://input"), true);

	$venta = Ventas::find($data['origen']['id']);
	
	$venta->estadoVenta=1;

	$venta->save();

	$nombrecomprador=$data['nombrecomprador'];
	$apellidocomprador=$data['apellidocomprador'];
	$correovendedor = array( 'correovendedor' => $data['correovendedor'] );	

    Mail::send('confirmarvendedor',$data, function( $message ) use ($correovendedor,$nombrecomprador,$apellidocomprador){
	
    $message->to($correovendedor['correovendedor'] )->subject('Se confirmo el contrato');
  
	});

	

	$nombrevendedor=$data['nombrevendedor'];
	$apellidovendedor=$data['apellidovendedor'];

	$correocomprador = array( 'correocomprador' => $data['correocomprador'] );	

    Mail::send('confirmadocomprador',$data, function( $message ) use ($correocomprador,$nombrevendedor,$apellidovendedor){
	
    $message->to($correocomprador['correocomprador'] )->subject('Se confirmo el contrato');
  
	});


	}
	public function postRechazarestadodeventa (){

	$data = json_decode(file_get_contents("php://input"), true);

	$venta = Ventas::find($data['origen']['id']);
	
	$venta->estadoVenta=2;

	$venta->save();

	$nombrecomprador=$data['nombrecomprador'];
	$apellidocomprador=$data['apellidocomprador'];
	$correovendedor = array( 'correovendedor' => $data['correovendedor'] );	

    Mail::send('rechazarvendedor',$data, function( $message ) use ($correovendedor,$nombrecomprador,$apellidocomprador){
	
    $message->to($correovendedor['correovendedor'] )->subject('Se rechazo el contrato');
  
	});

	

	$nombrevendedor=$data['nombrevendedor'];
	$apellidovendedor=$data['apellidovendedor'];

	$correocomprador = array( 'correocomprador' => $data['correocomprador'] );	

    Mail::send('rechazarcomprador',$data, function( $message ) use ($correocomprador,$nombrevendedor,$apellidovendedor){
	
    $message->to($correocomprador['correocomprador'] )->subject('Se rechazo el contrato');
  
	});


	}	

	public function postLiberarventa()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liberar_venta = DB::table('Ventas as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idComprador')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaVenta')
	        ->where('a.idVendedor',$data)
	        ->where('a.estadoVenta',1)
	        ->where('c.dominio','simbolo_moneda')
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto','c.valor')
	        ->get();
		return $liberar_venta;

	}

	public function postLiberarventadetalle()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liberar_venta_detail = DB::table('Ventas as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idComprador')
	        ->where('a.idVenta',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idVenta','a.estadoVenta','a.fechaVenta','a.idComprador','a.idVendedor','a.monedaVenta','a.montoVenta','a.idProducto')
	        ->get();

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($liberar_venta_detail)> 0) {
		$retObj -> estado = true;
		$retObj -> liberado = $liberar_venta_detail[0];

	}
	     return json_encode($retObj) ;
		

	}

	public function postActualizarliberardeventa (){

	$data = json_decode(file_get_contents("php://input"), true);

	$liberar = Ventas::find($data['idventa'] ['id']);
	
	$liberar->estadoVenta=7;
	$liberar->comentarioVenta=$data['observacion'];

	$liberar->save();

	}

	//talquilo
	public function postResumenalquilerrecibidosoles()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Alquileres')
	    ->where('idArrendatario',$data)
        ->where('monedaAlquiler','S')
        ->where('estado','<>','2')
        ->where('estado','=','1')
	    ->sum('montoAlquiler')
;		return $resumen_data;

	}
	public function postResumenalquilerrecibidodolar()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Alquileres')
	    ->where('idArrendatario',$data)
        ->where('monedaAlquiler','D')
        ->where('estado','<>','2')
        ->where('estado','=','1')
	    ->sum('montoAlquiler');
		return $resumen_data;

	}
	public function postResumenalquilerrealizadosoles()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Alquileres')
	    ->where('idArrendador',$data)
        ->where('monedaAlquiler','S')
        ->where('estado','<>','2')
        ->where('estado','=','1')
	    ->sum('montoAlquiler');
		return $resumen_data;

	}
	public function postResumenalquilerrealizadodolar()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$resumen_data = DB::table('Alquileres')
	    ->where('idArrendador',$data)
        ->where('monedaAlquiler','D')
        ->where('estado','<>','2')
        ->where('estado','=','1')
	    ->sum('montoAlquiler');
		return $resumen_data;

	}

	public function postDetallealquiler()
	{
		$data = json_decode(file_get_contents("php://input"), true);

		

		if ($data['moneda']['moneda'] == 1) {
		    $detalle_data = DB::table('Alquileres as a')
		    ->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendador')
		    ->join('Parametros as c', 'c.codigo', '=', 'a.monedaAlquiler')
	        ->where('a.idArrendatario',$data['usuario'])
            ->where('a.monedaAlquiler','S')
            ->where('a.estado','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.valor')
	   	    ->get();
		    return $detalle_data;	
		}else 
		if ($data['moneda'] ['moneda'] == 2) {
		   $detalle_data = DB::table('Alquileres as a')
		   ->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendador')
		   ->join('Parametros as c', 'c.codigo', '=', 'a.monedaAlquiler')
	       ->where('a.idArrendatario',$data['usuario'])
           ->where('a.monedaAlquiler','D')
           ->where('a.estado','<>','2')
           ->where('c.dominio','simbolo_moneda')
           ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.valor')
	       ->get();
		   return $detalle_data;
		}else
		if ($data['moneda'] ['moneda'] == 3) {
			$detalle_data = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendatario')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaAlquiler')
	        ->where('a.idArrendador',$data['usuario'])
            ->where('a.monedaAlquiler','S')
            ->where('a.estado','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.valor')
	        ->get();
		    return $detalle_data;
			
		}else
		if ($data['moneda'] ['moneda'] == 4) {
			$detalle_data = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendatario')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaAlquiler')
	        ->where('a.idArrendador',$data['usuario'])
            ->where('a.monedaAlquiler','D')
            ->where('a.estado','<>','2')
            ->where('c.dominio','simbolo_moneda')
            ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.valor')
	        ->get();
		    return $detalle_data;
			
		}
	}

	public function postObtenerdatospordnialquiler()
	{
		$data = file_get_contents("php://input");
		
		
		$user_data= Usuarios::where("idUsuario","=", $data )
		->selectRaw('CONCAT(nombresUsuario, " ", apellidosUsuario) as nombresUsuarios,idUsuario,nombresUsuario,apellidosUsuario,direccionUsuario,idDepartamento,idProvincia,idDistrito,celularUsuario,dniUsuario,correoUsuario,fechaNacimiento,sexoUsuario,tipoDocumento')
		->get() ;
		$retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($user_data)> 0) {
		$retObj -> estado = true;
		$retObj -> arrendatario = $user_data[0];

	}
	     return json_encode($retObj) ;

	}

	public function postObteneralquilerusuario(
)	{
		$data = json_decode(file_get_contents("php://input"), true);
	
		$user_alquiler = DB::table('Alquileres as a')
		->where('a.idArrendatario',$data)
		->where('a.estado',1)
        ->join('usuarios as b', 'a.idArrendador', '=', 'b.idUsuario')
        ->join('Productos as c','c.idProducto','=','a.productoAlquiler')
        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.nombreProducto')
        ->get();
        /*$user_alquiler[0]->fechaPagoPrestamo=date_format(date_create($user_alquiler[0]->fechaPagoPrestamo), 'd/m/Y');*/

        return $user_alquiler;

	}

	public function postGuardaralquiler()
	{
		$data = json_decode(file_get_contents("php://input"), true);

		//return $data['beneficiario']['dniUsuario'] ;	
			//return $data;

			$alquiler =  new Alquileres;
		    $alquiler->idArrendatario = $data['arrendatario']['idUsuario'];
		    $alquiler->idArrendador = $data['arrendador']['idUsuario'];
		    
		  
		    $alquiler->productoAlquiler = $data['productos'] ['idProducto'];
		    $alquiler->periodicidad = $data['periodoplazo']['idPeriodicidad'];
		    $alquiler->periodicidadMonto = $data['periodopago'] ['idPeriodicidad'];
		    $alquiler->montoAlquiler = $data['monto'];
		    $alquiler->monedaAlquiler = $data['moneda'] ['codigo'];
		    $alquiler->garantia = $data['garantia'];
		    $alquiler->referencia = $data['referencia'];
		    $alquiler->estado = 0;

		    $conversion= $data['inicio'];
		    $date = DateTime::createFromFormat('d/m/Y', $conversion);
		    $alquiler->inicio = $date;

		    $conversion= $data['plazo'];
		    $date = DateTime::createFromFormat('d/m/Y', $conversion);
		    $alquiler->plazo = $date;
		    

		    $alquiler->save();

		    

		    $pagosalquiler = new PagosAlquiler;

		    $pagosalquiler->idAlquileres=$alquiler->idAlquiler;
		  
		    $pagosalquiler->estadoPagoAlquiler = 3;
		  
		    $conversion= $data['inicio'];
		    $date = DateTime::createFromFormat('d/m/Y', $conversion);
		    $pagosalquiler->fechaVencimientoPagoAlquiler =$date;
		  
		    $pagosalquiler->save();
		    


			return 'true';

		
	}

	public function postConfirmaralquiler()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$confirmar_alquiler = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendador')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaAlquiler')
	        ->where('a.idArrendatario',$data)
	        ->where('a.estado',0)
	        ->where('c.dominio','simbolo_moneda')
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.valor')
	        ->get();
		return $confirmar_alquiler;
	
	}

	public function postConfirmaralquilerdetalle()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$confirmar_alquiler_detail = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendador')
			->join('Productos as c','c.idProducto','=','a.productoAlquiler')
			->join('Periodicidades as d','d.idPeriodicidad','=','a.periodicidadMonto')
			->join('Periodicidades as e','e.idPeriodicidad','=','a.periodicidad')
			->join('Parametros as f','f.codigo','=','a.monedaAlquiler')
	        ->where('a.idAlquiler',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','b.correoUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.nombreProducto','d.nombrePeriodicidad','e.nombrePeriodicidad as periodo','f.valor')
	        ->get();

	   $confirmar_alquiler_detail[0]->inicio=date_format(date_create($confirmar_alquiler_detail[0]->inicio), 'd/m/Y');

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($confirmar_alquiler_detail)> 0) {
		$retObj -> estado = true;
		$retObj -> confirmado = $confirmar_alquiler_detail[0];

	}
	     return json_encode($retObj) ;
		
		

	}

	public function postActualizarestadodealquiler (){

	$data = json_decode(file_get_contents("php://input"), true);

	$venta = Alquileres::find($data['id']);
	
	$venta->estado = 1;

	$venta->save();

	$cuotas = PagosAlquiler::where('idAlquileres', '=', $data['id'])
	->update(array('estadoPagoAlquiler' => 4));


	/*$nombrearrendatario=$data['nombrearrendatario'];
	$apellidoarrendatario=$data['apellidoarrendatario'];
	$correoarrendador = array( 'correoarrendador' => $data['correoarrendador'] );	

    Mail::send('confirmadoarrendador',$data, function( $message ) use ($correoarrendador,$nombrearrendatario,$apellidoarrendatario){
	
    $message->to($correoarrendador['correoarrendador'] )->subject('Se confirmo el contrato');
  
	});

	

	$nombrearrendador=$data['nombrearrendador'];
	$apellidoarrendador=$data['apellidoarrendador'];

	$correoarrendatario = array( 'correoarrendatario' => $data['correoarrendatario'] );	

    Mail::send('confirmararrendatario',$data, function( $message ) use ($correoarrendatario,$nombrearrendador,$apellidoarrendador){
	
    $message->to($correoarrendatario['correoarrendatario'] )->subject('Se confirmo el contrato');
  
	});*/


	}

	public function postRechazarestadodealquiler (){

	$data = json_decode(file_get_contents("php://input"), true);

	$venta = Alquileres::find($data['origen'] ['id']);
	
	$venta->estado=2;

	$venta->save();

	$nombre=$data['nombre'];
	$apellido=$data['apellido'];

	$correo = array( 'correo' => $data['correo'] );	

    Mail::send('rechazararrendatario',$data, function( $message ) use ($correo,$nombre,$apellido){
	
    $message->to($correo['correo'] )->subject('Rechazo de contrato');
  
	});

	}

	public function postLiberaralquiler()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liberar_alquiler = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendatario')
			->join('Parametros as c', 'c.codigo', '=', 'a.monedaAlquiler')
	        ->where('a.idArrendador',$data)
	        ->where('a.estado',1)
	        ->where('c.dominio','simbolo_moneda')
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.valor')
	        ->get();
		return $liberar_alquiler;

	}

	public function postLiberaralquilerdetalle()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$liberar_alquiler_detail = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendatario')
	        ->where('a.idAlquiler',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado')
	        ->get();

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($liberar_alquiler_detail)> 0) {
		$retObj -> estado = true;
		$retObj -> liberado = $liberar_alquiler_detail[0];

	}
	     return json_encode($retObj) ;
		

	}

	public function postActualizarliberardealquiler (){

	$data = json_decode(file_get_contents("php://input"), true);

	$liberar = Alquileres::find($data['idalquiler']['id']);
	
	$liberar->estado=7;
	$liberar->comentarioAlquiler=$data['observacion'];

	$liberar->save();

	}

	public function postObtenerdatospordnialquilerr()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$data_alquiler = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendatario')
			->where('b.idUsuario',$data['idarrendatario'])
	        ->where('a.idArrendador',$data['idarrendador'])
	        ->where('a.idAlquiler',$data['idalquiler'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','b.correoUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado')

	        ->get();



	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($data_alquiler)> 0) {
		$retObj -> estado = true;
		$retObj -> dnialquiler = $data_alquiler[0];

	}
	     return json_encode($retObj) ;
		

	}

	public function postObteneralquiler()
	{
		

		$data = json_decode(file_get_contents("php://input"), true);

		$alquiler = DB::table('PagosAlquiler as a')

			->join('Alquileres as b', 'b.idAlquiler', '=', 'a.idAlquileres')
			->join('Parametros as c', 'a.monedaPagoAlquiler', '=', 'c.codigo')
	        ->where('a.idAlquileres',$data)
	        ->where('c.dominio','=','simbolo_moneda')
	        ->where('a.estadoPagoAlquiler',5)

	        ->get();

	  /* $confirmar_alquiler_detail[0]->inicio=date_format(date_create($confirmar_alquiler_detail[0]->inicio), 'd/m/Y');*/	
		return $alquiler;

	}

	public function postListardetalleliquidaralquiler()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$data_alquiler = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendatario')
			->join('PagosAlquiler as c', 'c.idAlquileres', '=', 'a.idAlquiler')
	        ->where('c.idPagoAlquiler',$data['id'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado','c.estadoPagoAlquiler','c.fechaPagoAlquiler','c.fechaVencimientoPagoAlquiler','c.formaPagoAlquiler','c.idAlquileres','c.idPagoAlquiler','c.monedaPagoAlquiler','montoPagoAlquiler')
	        ->get();

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($data_alquiler)> 0) {
		$retObj -> estado = true;
		$retObj -> liquidar = $data_alquiler[0];

	}
	     return json_encode($retObj) ;
		



	}


	public function postActualizarliquidaralquiler (){

	$data = json_decode(file_get_contents("php://input"), true);

	$liquidar = PagosAlquiler::find($data['idalquiler'] ['id']);
	$liquidar->comentario=$data['observacion'];
	$liquidar->estadoPagoAlquiler=6;

	$liquidar->save();

	$nombre=$data['nombre'];
	$apellido=$data['apellido'];
	

	$correo = array( 'correo' => $data['correo'] );	

    Mail::send('liquidezalquiler',$data, function( $message ) use ($correo,$nombre,$apellido){
	
    $message->to($correo['correo'] )->subject('Liquidez de alquiler');
  
	});

	}

	//
	public function postObtenerdatospordnialquileres()
	
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$data_alquiler = DB::table('Alquileres as a')
			->join('usuarios as b', 'b.idUsuario', '=', 'a.idArrendador')

			->where('b.idUsuario',$data['nombrearrendador'])
	        ->where('a.idArrendatario',$data['idarrendatario'])
	        ->where('a.idAlquiler',$data['idalquiler'])
	        ->select('b.idUsuario','b.nombresUsuario','b.apellidosUsuario','b.correoUsuario','a.idAlquiler','a.garantia','a.idArrendador','a.idArrendatario','a.inicio','a.monedaAlquiler','a.montoAlquiler','a.periodicidad','a.periodicidadMonto','a.plazo','a.productoAlquiler','a.referencia','a.estado')
	        ->get();

	    $retObj = new stdClass();
	    $retObj -> estado = false;

		if (count($data_alquiler)> 0) {
		$retObj -> estado = true;
		$retObj -> pagaralquiler = $data_alquiler[0];

	}
	     return json_encode($retObj) ;
		

	
		
		

	}

	public function postObtenerpagosalquiler()
	{
		

		$data = json_decode(file_get_contents("php://input"), true);
		$pagos_alquiler = DB::table('PagosAlquiler as a')
			->join('Alquileres as b', 'b.idAlquiler', '=', 'a.idAlquileres')
			->join('Parametros as c', 'b.monedaAlquiler', '=', 'c.codigo')
	        ->where('a.idAlquileres','=',$data)
	        ->where('c.dominio','=','simbolo_moneda')
	        ->where('a.estadoPagoAlquiler','=',4)
	        ->select('a.comentario','a.estadoPagoAlquiler','a.fechaPagoAlquiler','a.fechaVencimientoPagoAlquiler','a.formaPagoAlquiler','a.idAlquileres','a.idPagoAlquiler','b.monedaAlquiler','b.montoAlquiler','c.valor')
	        ->get();

		return $pagos_alquiler;



	}


	public function postPagaralquiler()
	{
		
		$data = json_decode(file_get_contents("php://input"), true);
		$pagoalquiler = PagosAlquiler::find($data['idpagoalquiler'] ['id']);
		$pagoalquiler->estadoPagoAlquiler = 5;
		$pagoalquiler->formaPagoAlquiler=$data['pago']['forma'];
		$pagoalquiler->monedaPagoAlquiler=$data['pago']['moneda'];
		$pagoalquiler->montoPagoAlquiler=$data['pago']['monto'];
		$conversion= $data['pago']['fecha'];
		$date = DateTime::createFromFormat('d/m/Y', $conversion);
		$pagoalquiler->fechaPagoAlquiler = $date;
		$pagoalquiler->save();

		$nombre=$data['nombre'];
		$apellido=$data['apellido'];
	

		$correo = array( 'correo' => $data['correo'] );	

        Mail::send('pagoalquiler',$data, function( $message ) use ($correo,$nombre,$apellido){
	
        $message->to($correo['correo'] )->subject('Pago de alquiler');
  
	    });
	}

}
