<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Twilio Account SID
    |--------------------------------------------------------------------------
    |
    | The 34 character string that uniquely identifies the Twilio account. 
    | It can be found right under the main navigation after logging into Twilio.
    |
    */
    'sid'   => 'ACcdd57c67fa287d35ddd671baca283fce',

    /*
    |--------------------------------------------------------------------------
    | Twilio Auth Token
    |--------------------------------------------------------------------------
    |
    | The authorization token for the specific Twilio account.
    | This token should be kept a secret.
    |
    */
    'token' => '4377f5bc6f7a29fc051af6db2d4cb633',

    /*
    |--------------------------------------------------------------------------
    | Twilio From Phone Number
    |--------------------------------------------------------------------------
    |
    | Enter your registered Twilio Phone Number from where all SMS and Calls
    | will be sent.
    |
    */
    'from'  => '+17042287458'
);
